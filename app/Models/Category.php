<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Category extends Model
{
    use HasFactory;
    use SoftDeletes;

    protected $table = 'categories';
    protected $guarded = [];

    public function models()
    {
        return $this->hasMany(ModelMoto::class);
    }

    public function stock()
    {
        return $this->hasManyThrough('App\Models\Stock', 'App\Models\ModelMoto', 'category_id', 'model_moto_id', 'id', 'id');
    }

    public function getOneImage($images){
        $images = explode(';', $images);
        return array_pop($images);
    }
}

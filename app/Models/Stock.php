<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Stock extends Model
{
    use HasFactory;
    protected $table = 'stocks';
    protected $guarded = [];

    public function model(){
        return $this->belongsTo(ModelMoto::class, 'model_moto_id', 'id');
    }

    public function getColorName(){
        $color =  Color::where('id', $this->color_id)->pluck('title');
        $color = json_encode($color, JSON_UNESCAPED_UNICODE);
        return preg_replace('/[^a-zа-я ]/ui', '', $color);
    }
}

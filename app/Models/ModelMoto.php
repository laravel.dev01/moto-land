<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ModelMoto extends Model
{
    use HasFactory;

    protected $table = 'model_motos';
    protected $guarded = [];

    public function tths()
    {
        return $this->hasMany(Tth::class);
    }

    public function stock()
    {
        return $this->hasMany(Stock::class);
    }
}

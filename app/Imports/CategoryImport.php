<?php

namespace App\Imports;

use App\Models\Category;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithHeadingRow;

class CategoryImport implements ToModel, WithHeadingRow
{
//    protected $id;
//
//    function __construct($id)
//    {
//        $this->id = $id;
//    }

    public function model(array $row)
    {
        $res = Category::pluck('category_name')->toArray();
        if (!in_array($row['title'], $res)) {
            return new Category([
                'category_name' => $row['title']
            ]);
        }
    }
}
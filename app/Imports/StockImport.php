<?php

namespace App\Imports;

use App\Models\Stock;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithHeadingRow;

class StockImport implements ToModel, WithHeadingRow
{
//    protected $id;
//
//    function __construct($id)
//    {
//        $this->id = $id;
//    }

    public function model(array $row)
    {
        return new Stock([
            'vin'           => $row['vin'],
            'engine'        => $row['engine'],
            'name'          => $row['name'],
            'color_id'      => $row['color_id'],
            'options'       => $row['options'],
            'model_moto_id' => $row['model_moto_id'],
            'price'         => $row['price'],
            'special_price' => $row['special_price'],
            'trade_in'      => $row['trade_in'],
            'kasko'         => $row['kasko'],
            'credit'        => $row['credit'],
            'leasing'       => $row['leasing'],
            'offer'         => $row['offer'],
            'status'        => $row['status'],
        ]);
    }
}
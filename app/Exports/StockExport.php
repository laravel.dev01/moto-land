<?php

namespace App\Exports;

use App\Models\Category;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\FromQuery;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Illuminate\Support\Facades\DB;

class StockExport implements FromCollection, WithHeadings, ShouldAutoSize
{

    /**
     * @return \Illuminate\Support\Collection
     */
    public function collection()
    {
        return DB::table('stocks')->select('vin', 'engine', 'name', 'color_id', 'options', 'model_moto_id', 'price', 'special_price', 'trade_in', 'kasko', 'credit', 'leasing', 'offer', 'status')->get();
    }

    public function headings(): array
    {
        return [
            'vin', 'engine', 'name', 'color_id', 'options', 'model_moto_id', 'price', 'special_price', 'trade_in', 'kasko', 'credit', 'leasing', 'offer', 'status',
        ];
    }
}

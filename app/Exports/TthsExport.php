<?php

namespace App\Exports;

use App\Models\Tth;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\FromQuery;
use Maatwebsite\Excel\Concerns\WithHeadings;

class TthsExport implements FromCollection, WithHeadings
{
    protected $id;

    function __construct($id) {
        $this->id = $id;
    }
    /**
     * @return \Illuminate\Support\Collection
     */
    public function collection()
    {
        return Tth::where('id',$this->id)->get();
    }

    public function headings(): array
    {
        return [
            "ID",
            "Model_moto_id",
            "Объем",
            "Мощность",
            "Крутящий момент",
            "Количество цилиндров / клапанов на цилиндр",
            "Максимальная скорость",
            "Расход",
            "Собственная масса ",
            "Допустимая полная масса",
            "Created_at",
            "Updated_at",
        ];
    }
}

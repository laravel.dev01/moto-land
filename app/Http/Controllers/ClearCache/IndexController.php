<?php

namespace App\Http\Controllers\ClearCache;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class IndexController extends Controller
{
    public function __invoke()
    {
        exec ( 'php artisan cache:clear' ) ;
        exec ( 'php artisan config:clear' ) ;
        exec ( 'php artisan route:clear' ) ;
        exec ( 'php artisan view:clear' ) ;


        return redirect()->route('admin.index');
    }
}

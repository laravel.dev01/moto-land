<?php

namespace App\Http\Controllers\Site\Main;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class IndexController extends Controller
{
    public function __invoke()
    {
//        return view('welcome');
        return view('site.main.index');
    }
}

<?php

namespace App\Http\Controllers\Manager\Application;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class IndexController extends Controller
{
    public function __invoke()
    {
        $data['title'] = 'Заявки';
        $data['breadcrumbs'] = [
            [
                'text' => 'Главная',
                'href' => 'https://moto-land.space/manager'
            ],
            [
                'text' => 'Заявки',
                'href' => 'https://moto-land.space/applications'
            ],
        ];

        return view('manager.applications.index', compact('data'));
    }
}

<?php

namespace App\Http\Controllers\Manager\Stock;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class IndexController extends Controller
{
    public function __invoke()
    {
        $data['title'] = 'Склад';
        $data['breadcrumbs'] = [
            [
                'text' => 'Главная',
                'href' => 'https://moto-land.space/manager'
            ],
            [
                'text' => 'Склад',
                'href' => 'https://moto-land.space/manager/stock'
            ],
        ];
        return view('manager.stock.index', compact('data'));
    }
}

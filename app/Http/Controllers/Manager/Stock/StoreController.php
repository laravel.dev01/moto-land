<?php

namespace App\Http\Controllers\Manager\Stock;

use App\Http\Controllers\Controller;
use App\Models\Stock;
use Illuminate\Http\Request;

class StoreController extends Controller
{

    public function __invoke()
    {
        $stocks = Stock::all();

        foreach ($stocks as $stock){
            $stock['image'] = $stock->model['image'];
        }

        return json_encode($stocks);
    }
}

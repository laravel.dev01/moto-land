<?php

namespace App\Http\Controllers\Manager\Main;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class IndexController extends Controller
{
    public function __invoke()
    {
        $data['title'] = 'Главная';
        $data['breadcrumbs'] = [
            [
                'text' => 'Главная',
                'href' => 'https://moto-land.space/manager'
            ]
        ];

        return view('manager.main.index', compact('data'));
    }
}

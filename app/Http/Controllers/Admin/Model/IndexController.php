<?php

namespace App\Http\Controllers\Admin\Model;

use App\Http\Controllers\Controller;
use App\Models\Category;
use App\Models\ModelMoto;


class IndexController extends Controller
{
    public function __invoke()
    {
        $data['title'] = 'Модели';
        $data['breadcrumbs'] = [
            [
                'text' => 'Главная',
                'href' => 'https://moto-land.space/admin/categories'
            ],
            [
                'text' => 'Модели',
                'href' => 'https://moto-land.space/admin/models'
            ],
        ];

        $categories = Category::all();
        return view('admin.models.index', compact('categories', 'data'));
    }


}

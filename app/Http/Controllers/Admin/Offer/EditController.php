<?php

namespace App\Http\Controllers\Admin\Offer;

use App\Http\Controllers\Controller;
use App\Models\Offer;
use Illuminate\Http\Request;

class EditController extends Controller
{
    public function __invoke(Offer $offer)
    {
        return view('admin.offer.edit', compact('offer'));
    }
}

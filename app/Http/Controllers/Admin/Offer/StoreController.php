<?php

namespace App\Http\Controllers\Admin\Offer;

use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\Offer\StoreRequest;
use App\Models\Offer;
use Illuminate\Http\Request;

class StoreController extends Controller
{
    public function __invoke(StoreRequest $request)
    {
        $data = $request->validated();
        Offer::updateOrCreate(['name' => $data['name']], $data);
        return $data;
    }
}

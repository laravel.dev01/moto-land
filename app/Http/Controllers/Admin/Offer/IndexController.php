<?php

namespace App\Http\Controllers\Admin\Offer;

use App\Http\Controllers\Controller;
use App\Models\Offer;
use Illuminate\Http\Request;

class IndexController extends Controller
{
    public function __invoke()
    {
        $offers = Offer::all();
        return view('admin.offer.index', compact('offers'));
    }
}

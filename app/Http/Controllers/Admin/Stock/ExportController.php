<?php

namespace App\Http\Controllers\Admin\Stock;

use App\Exports\StockExport;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Maatwebsite\Excel\Facades\Excel;

class ExportController extends Controller
{
    public function __invoke()
    {
        return Excel::download(new StockExport, 'stock-' . date('d-m-Y') . '-' . 'export' . '.xlsx');
    }
}

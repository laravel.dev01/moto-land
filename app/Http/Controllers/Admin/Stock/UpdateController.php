<?php

namespace App\Http\Controllers\Admin\Stock;

use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\Stock\UpdateRequest;
use App\Models\Stock;
use Illuminate\Http\Request;

class UpdateController extends Controller
{
    public function __invoke(UpdateRequest $request)
    {
        $res = $request->validated();
        Stock::updateOrCreate(['vin' => $res['vin']], $res);
        $data = Stock::where(['vin' => $res['vin']])->first();
        return $data;
    }
}

<?php

namespace App\Http\Controllers\Admin\Stock;

use App\Http\Controllers\Controller;
use App\Models\Stock;
use Illuminate\Http\Request;

class IndexController extends Controller
{
    public function __invoke()
    {
        $data['title'] = 'Склад';
        $data['breadcrumbs'] = [
            [
                'text' => 'Главная',
                'href' => 'https://moto-land.space/admin/categories'
            ],
            [
                'text' => 'Склад',
                'href' => 'https://moto-land.space/admin/stock'
            ],
        ];

        return view('admin.stock.index', compact('data'));
    }
}

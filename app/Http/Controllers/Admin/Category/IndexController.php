<?php

namespace App\Http\Controllers\Admin\Category;

use App\Http\Controllers\Controller;
use App\Models\Category;
use Illuminate\Http\Request;

class IndexController extends Controller
{
    public function __invoke()
    {
        $data['title'] = 'Классы';
        $data['breadcrumbs'] = [
            [
                'text' => 'Классы',
                'href' => 'https://moto-land.space/admin/categories'
            ]
        ];
        $categories = Category::all();
        foreach ($categories as $category) {
            $category['models'] = $category->models;
            $category['stock'] = $category->stock;
            $category['data'] = date("d-m-Y", strtotime($category->created_at));
        }

        return view('admin.categories.index', compact('categories', 'data'));
    }
}

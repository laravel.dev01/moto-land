<?php

namespace App\Http\Controllers\Admin\Category;

use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\Category\UpdateRequest;
use App\Models\Category;
use Illuminate\Http\Request;
use mysql_xdevapi\Exception;

class UpdateController extends Controller
{
    public function __invoke(Category $category)
    {
        try {
            $res = $category->update();
        } catch (Exception $e) {
            echo 'Erorr: ', $e->getMessage(), "\n";
        }
        if ($res) {
            $data = [
                'status'  => 'success',
                'message' => 'Обновлена категория ' . $category->category_name
            ];
        } else {
            $data = [
                'status'  => 'fail',
                'message' => 'Error ' . $e->getMessage(), "\n"
            ];
        }
        return response()->json($data);
    }
//    public function __invoke(Category $category)
//    {
//        $data['title'] = 'Добавление класса';
//        $data['breadcrumbs'] = [
//            [
//                'text' => 'Классы',
//                'href' => 'https://moto-land.space/admin/categories'
//            ],
//            [
//                'text' => 'Добавление класса',
//                'href' => 'https://moto-land.space/admin/categories/create'
//            ],
//        ];
//
//        return view('admin.categories.update', compact('category', 'data'));
//    }
}

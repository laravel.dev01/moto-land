<?php

namespace App\Http\Controllers\Admin\Category;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class CreateController extends Controller
{
    public function __invoke()
    {
        $data['title'] = 'Добавление класса';
        $data['breadcrumbs'] = [
            [
                'text' => 'Классы',
                'href' => 'https://moto-land.space/admin/categories'
            ],
            [
                'text' => 'Добавление класса',
                'href' => 'https://moto-land.space/admin/categories/create'
            ],
        ];

        return view('admin.categories.create', compact('data'));
    }
}

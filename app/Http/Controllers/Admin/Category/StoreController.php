<?php

namespace App\Http\Controllers\Admin\Category;

use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\Category\StoreRequest;
use App\Models\Category;
use App\Models\ModelMoto;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class StoreController extends Controller
{
    public function __invoke(StoreRequest $request)
    {
        $data = $request->validated();
        $modelName = null;
        $modelImage = null;

        if (!empty($data['model_name'])) {
            $modelName = $data['model_name'];
            unset($data['model_name']);
        }
        if (!empty($data['model_image'])) {
            $modelImage = $data['model_image'];
            unset($data['model_image']);
            $modelImage = Storage::put('/public/images', $modelImage);
            $modelImage = explode('/', $modelImage);
            $modelImage = array_pop($modelImage);
            $modelImage = 'https://' . $_SERVER['HTTP_HOST'] . '/public/storage/images/' . $modelImage;
        }

        Category::updateOrCreate(['category_name' => $data['category_name']], $data);

        $imageStock = 'https://' . $_SERVER['HTTP_HOST'] . '/public/assets/images/no-image.png';
        if ($modelName) {
            $image = $modelImage ?? $imageStock;
            $categoryId = Category::where(['category_name' => $data['category_name']])->value('id');
            $modelImagesFromBd = ModelMoto::where(['name' => $modelName])->value('image');

            if (!empty($modelImagesFromBd)) {
                $modelImagesFromBd = explode(';', $modelImagesFromBd);
                if (in_array($image, $modelImagesFromBd)) {
                    return false;
                }
                array_push($modelImagesFromBd, $image);
                $image = implode(';', $modelImagesFromBd);
            }
            $dataModel = [
                'name'        => mb_strtoupper($modelName),
                'category_id' => $categoryId,
                'image'       => $image
            ];
            ModelMoto::updateOrCreate(['name' => $modelName], $dataModel);
        }
        if ($modelImage) {
            $data = [
                'status' => 'success',
                'image'  => $modelImage
            ];
        } else {
            $data = [
                'status' => 'success',
                'image'  => $imageStock
            ];
        }
        return response()->json($data);
    }

    public function delete(StoreRequest $request)
    {
        $data = $request->validated();
        $res = ModelMoto::where('image', $data['model_image_src'])->delete();
        if ($res) {
            $data = [
                'status' => 'success'
            ];
        } else {
            $data = [
                'status' => 'fail'
            ];
        }
        return response()->json($data);
    }
    public function updateCategory(StoreRequest $request)
    {
        $data = $request->validated();
        $res = Category::where('id', $data['category_id'])->update(['category_status' => $data['category_status']]);
        if ($res) {
            $data = [
                'status' => 'success'
            ];
        } else {
            $data = [
                'status' => 'fail'
            ];
        }
        return response()->json($data);
    }
    public function editCategory(StoreRequest $request)
    {
        $data = $request->validated();
        $res = Category::where('id', $data['category_id'])->update(['category_status' => $data['category_status']]);
        if ($res) {
            $data = [
                'status' => 'success'
            ];
        } else {
            $data = [
                'status' => 'fail'
            ];
        }
        return response()->json($data);
    }
}
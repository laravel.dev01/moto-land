<?php

namespace App\Http\Controllers\Admin\Category;

use App\Exports\CatogorysExport;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Maatwebsite\Excel\Facades\Excel;

class ExportController extends Controller
{
    public function __invoke()
    {
        return Excel::download(new CatogorysExport, 'category-' . date('d-m-Y') . '-' . 'export' . '.xlsx');
    }
}

<?php

namespace App\Http\Controllers\Admin\Category;

use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\Category\UpdateRequest;
use App\Models\Category;
use Illuminate\Http\Request;
use mysql_xdevapi\Exception;

class DeleteController extends Controller
{
    public function __invoke(Category $category)
    {
        try {
            $res = $category->delete();
        } catch (Exception $e) {
            echo 'Erorr: ', $e->getMessage(), "\n";
        }
        if ($res) {
            $data = [
                'status'  => 'success',
                'message' => 'Удалена категория ' . $category->category_name
            ];
        } else {
            $data = [
                'status'  => 'fail',
                'message' => 'Error ' . $e->getMessage(), "\n"
            ];
        }
        return response()->json($data);
    }
}

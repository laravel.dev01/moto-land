<?php

namespace App\Http\Controllers\Admin\Category;

use App\Http\Controllers\Controller;
use App\Imports\CategoryImport;
use Illuminate\Http\Request;
use Maatwebsite\Excel\Facades\Excel;


class ImportController extends Controller
{
    public function __invoke(Request $request)
    {
        $res = Excel::import(new CategoryImport, $request->file('import_xls'));
//        if ($res) {
//            $data = [
//                'status' => 'success'
//            ];
//        } else {
//            $data = [
//                'status' => 'fail'
//            ];
//        }
//        return response()->json($data);
        return back();
    }
}

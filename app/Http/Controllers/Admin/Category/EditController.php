<?php

namespace App\Http\Controllers\Admin\Category;

use App\Http\Controllers\Controller;
use App\Models\Category;
use Illuminate\Http\Request;

class EditController extends Controller
{
    public function __invoke(Category $category)
    {
        $data['title'] = 'Редактирование класса - ' . $category->category_name;
        $data['breadcrumbs'] = [
            [
                'text' => 'Классы',
                'href' => 'https://moto-land.space/admin/categories'
            ],
            [
                'text' => 'Редактирование класса',
                'href' => 'https://moto-land.space/admin/categories/create'
            ],
        ];

        $category['models'] = $category->models->toArray();
        $category = json_encode($category->toArray());

        return view('admin.categories.edit', compact('category', 'data'));
    }
}

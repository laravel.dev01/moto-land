<?php

namespace App\Http\Controllers\Admin\Tth;

use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\Ttx\StoreRequest;
use Illuminate\Http\Request;

class CreateController extends Controller
{
    public function __invoke(StoreRequest $request)
    {
        $data = $request->validate();
        dd($data);
        return view('admin.categories.create');
    }
}

<?php

namespace App\Http\Controllers\Admin\Tth;

use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\Tth\ExportRequest;
use App\Imports\TthsImport;
use App\Models\Tth;
use Illuminate\Http\Request;
use Maatwebsite\Excel\Facades\Excel;


class ImportController extends Controller
{
    public function __invoke(Request $request)
    {
        $tths = Excel::toCollection(new TthsImport($request->id), $request->file('import_xls'));


        Tth::create([
            'id'                 => $request->id,
            'model_moto_id'      => $tths[0][1][1],
            'engine_capacity'    => $tths[0][1][2],
            'engine_power'       => $tths[0][1][3],
            'torque'             => $tths[0][1][4],
            'cylinders_valves'   => $tths[0][1][5],
            'maximum_speed'      => $tths[0][1][6],
            'fuel_consumption'   => $tths[0][1][7],
            'its_mass'           => $tths[0][1][8],
            'permissible_weight' => $tths[0][1][9],
        ]);


        return redirect()->route('admin.models.index');
    }
}

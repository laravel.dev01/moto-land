<?php

namespace App\Http\Controllers\Admin\Tth;

use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\Tth\ExportRequest;
use App\Models\ModelMoto;
use Illuminate\Http\Request;
use App\Exports\TthsExport;
use Maatwebsite\Excel\Facades\Excel;

class ExportController extends Controller
{
    public function __invoke(Request $request)
    {
        $requestName = ModelMoto::where('id', $request->id)->pluck('name')->first();
        return Excel::download(new TthsExport($request->id), 'tth-' . date('d-m-Y') . '-' . $requestName . '.xlsx');
    }
}

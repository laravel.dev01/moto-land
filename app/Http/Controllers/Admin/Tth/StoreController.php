<?php

namespace App\Http\Controllers\Admin\Tth;

use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\Tth\ExportRequest;
use Illuminate\Http\Request;

class StoreController extends Controller
{
    public function __invoke(ExportRequest $request)
    {
        $data = $request->validated();


        $xls = PHPExcel_IOFactory::load($data['filename']);

        dd($xls);
        return view('admin.categories.create');
    }
}

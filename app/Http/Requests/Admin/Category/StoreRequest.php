<?php

namespace App\Http\Requests\Admin\Category;

use Illuminate\Foundation\Http\FormRequest;

class StoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'category_id'=>'nullable',
            'category_name'=>'nullable|string',
            'category_status'=>'nullable|boolean',
            'model_name'=>'nullable|string',
            'model_image'=>'nullable',
            'model_image_src'=>'nullable',
        ];
    }
}

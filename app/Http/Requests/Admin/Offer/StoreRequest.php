<?php

namespace App\Http\Requests\Admin\Offer;

use Illuminate\Foundation\Http\FormRequest;

class StoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'nullable',
            'date_start' => 'nullable',
            'date_end' => 'nullable',
            'title' => 'nullable',
            'short_description' => 'nullable',
            'full_description' => 'nullable',
            'meta_description' => 'nullable',
            'type' => 'nullable',
            'status' => 'nullable',
        ];
    }
}

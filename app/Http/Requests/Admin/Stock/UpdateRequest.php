<?php

namespace App\Http\Requests\Admin\Stock;

use Illuminate\Foundation\Http\FormRequest;

class UpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'vin' => 'nullable',
            'price' => 'nullable',
            'special_price' => 'nullable',
            'trade_in' => 'nullable',
            'kasko' => 'nullable',
            'credit' => 'nullable',
            'leasing' => 'nullable',
            'offer' => 'nullable',
            'status' => 'nullable',
        ];
    }
}
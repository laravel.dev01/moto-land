<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::group(['namespace'=>'Site'], function (){
    Route::group(['namespace'=>'Main'], function (){
        Route::get('/', 'IndexController')->name('site.main.index');
    });
});

Route::group(['namespace'=>'ClearCache', 'prefix'=>'clear_cache'], function (){
    Route::get('/', 'IndexController')->name('clear_cache.index');
});

// MANAGER

Route::group(['namespace'=>'Manager', 'prefix'=>'manager', 'middleware'=>['manager','auth']], function (){
    Route::group(['namespace'=>'Main'], function (){
        Route::get('/', 'IndexController')->name('manager.main.index');
    });
    Route::group(['namespace'=>'Application','prefix'=>'applications'], function (){
        Route::get('/', 'IndexController')->name('manager.applications.index');
        Route::get('/read', 'ReadController')->name('manager.applications.read');
    });
    Route::group(['namespace'=>'Stock','prefix'=>'stock'], function (){
        Route::get('/', 'IndexController')->name('manager.stock.index');
    });
});

// ADMIN

Route::group(['namespace'=>'Admin', 'prefix'=>'admin', 'middleware'=>['admin','auth']], function (){
    Route::group(['namespace'=>'Main'], function (){
        Route::get('/', 'IndexController')->name('admin.index');
    });

    Route::group(['namespace' => 'Category', 'prefix' => 'categories'], function () {
        Route::get('/', 'IndexController')->name('admin.category.index');
        Route::get('/create', 'CreateController')->name('admin.category.create');
        Route::post('/', 'StoreController')->name('admin.category.store');
        Route::get('/export', 'ExportController')->name('admin.category.export');
        Route::post('/import', 'ImportController')->name('admin.category.import');
//        Route::get('/{category}', 'ShowController')->name('admin.category.show');
        Route::get('/{category}/edit', 'EditController')->name('admin.category.edit');
        Route::patch('/{category}', 'UpdateController')->name('admin.category.update');
//        Route::delete('/{category}', 'DeleteController')->name('admin.category.delete');
    });
+
    Route::group(['namespace' => 'Model', 'prefix' => 'models'], function () {
        Route::get('/', 'IndexController')->name('admin.models.index');
//        Route::get('/create', 'CreateController')->name('admin.category.create');
//        Route::post('/', 'StoreController')->name('admin.category.store');
//        Route::get('/{category}', 'ShowController')->name('admin.category.show');
//        Route::get('/{category}/edit', 'EditController')->name('admin.category.edit');
//        Route::patch('/{category}', 'UpdateController')->name('admin.category.update');
//        Route::delete('/{category}', 'DeleteController')->name('admin.category.delete');
    });

    Route::group(['namespace' => 'Tth', 'prefix' => 'tth'], function () {
        Route::post('/import/{id}', 'ImportController')->name('admin.tth.import');
        Route::get('/{id}', 'ExportController')->name('admin.tth.export');
    });

    Route::group(['namespace' => 'Stock', 'prefix' => 'stock'], function () {
        Route::get('/', 'IndexController')->name('admin.stock.index');
        Route::get('/export', 'ExportController')->name('admin.stock.export');
        Route::post('/import', 'ImportController')->name('admin.stock.import');
    });

    Route::group(['namespace' => 'Offer', 'prefix' => 'offer'], function () {
        Route::get('/', 'IndexController')->name('admin.offer.index');
        Route::get('/create', 'CreateController')->name('admin.offer.create');
        Route::get('/{offer}/edit', 'EditController')->name('admin.offer.edit');
        Route::delete('/{offer}', 'DeleteController')->name('admin.offer.delete');
//        Route::post('/', 'StoreController')->name('admin.offer.store');
    });

});

Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');

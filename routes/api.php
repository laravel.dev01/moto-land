<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});

Route::group(['namespace'=>'Admin', 'prefix'=>'admin'], function (){

    Route::group(['namespace' => 'Offer', 'prefix' => 'offer'], function () {
//        Route::get('/', 'IndexController')->name('admin.offer.index');
        Route::post('/', 'StoreController');
    });

    Route::group(['namespace' => 'Stock', 'prefix' => 'stock'], function () {
//        Route::get('/', 'IndexController')->name('admin.offer.index');
        Route::get('/', 'StoreController');
        Route::post('/', 'UpdateController');
        Route::post('/search', 'SearchController');
    });

    Route::group(['namespace' => 'Category', 'prefix' => 'category'], function () {
        Route::post('/', 'StoreController');
        Route::post('/image-delete', 'StoreController@delete');
        Route::post('/category-delete/{category}', 'DeleteController');
        Route::post('/category-update/{category}', 'StoreController@updateCategory');
        Route::get('/category-edit/{category}', 'EditController');
    });
});

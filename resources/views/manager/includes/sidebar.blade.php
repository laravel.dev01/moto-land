<aside class="main-sidebar sidebar-dark-primary elevation-4">
    <!-- Brand Logo -->
    <div class="brand-link">
        <i style='font-size:24px; padding: 0 6px 0 20px;' class='fas'>&#xf508;</i>
        <span class="brand-text font-weight-light">Manager</span>
        <a href="{{ route('logout') }}" style="padding-left: 10px"
           onclick="event.preventDefault();
           document.getElementById('logout-form').submit();">
            выйти
        </a>
        <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
            @csrf
        </form>
    </div>
    <!-- Sidebar -->
    <div class="sidebar pt-3">
        <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="true">
            <li class="nav-item">
                <a href="{{ route('manager.main.index') }}" class="nav-link">
                    <i style="font-size:24px; padding: 0 5px 0 2px" class="fa">&#xf200;</i>
                    <p>
                        Главная
                    </p>
                </a>
            </li>
            <li class="nav-item">
                <a href="{{ route('manager.stock.index') }}" class="nav-link">
                    <i class="nav-icon fas fa-warehouse"></i>
                    <p>
                        Склад
                    </p>
                </a>
            </li>
            <li class="nav-item">
                <a href="{{ route('manager.applications.index') }}" class="nav-link d-flex">
                    <i class="nav-icon far fa-envelope" style="font-size: 23px"></i>
                    <p style="padding-left: 4px;">
                        Заявки
                    </p>
                </a>
            </li>
        </ul>
    </div>
    <!-- /.sidebar -->
</aside>

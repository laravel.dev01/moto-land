@extends('manager.layouts.main')
@section('title', $data['title'])
@section('content')
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <div class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <h1 class="m-0">{{$data['title']}}</h1>
                    </div><!-- /.col -->
                    <div class="col-sm-6">
                        <ol class="breadcrumb float-sm-right">
                            @foreach($data['breadcrumbs'] as $item)
                                @if(count($data['breadcrumbs'])>=2 && array_pop($data['breadcrumbs']))
                                    <li class="breadcrumb-item"><a href="{{$item['href']}}">{{$item['text']}}</a></li>
                                @else
                                    <li class="breadcrumb-item active">{{$item['text']}}</li>
                                @endif
                            @endforeach
                        </ol>
                    </div><!-- /.col -->
                </div><!-- /.row -->
            </div><!-- /.container-fluid -->
        </div>
        <!-- /.content-header -->

        <!-- Main content -->
        <section class="content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-8">
                        <div class="card">
                            <div class="card-header ui-sortable-handle" style="cursor: move;">
                                <h3 class="card-title">
                                    <i class="fas fa-chart-pie mr-1"></i>
                                    Посещения
                                </h3>
                            </div><!-- /.card-header -->
                            <visit-component></visit-component>
                        </div>
                    </div>
                    <div class="col-4">
                        <div class="card card-danger" style="padding-bottom: 56px">
                            <div class="card-header">
                                <h3 class="card-title">Pie Chart</h3>

                                <div class="card-tools">
                                    <button type="button" class="btn btn-tool" data-card-widget="collapse">
                                        <i class="fas fa-minus"></i>
                                    </button>
                                    <button type="button" class="btn btn-tool" data-card-widget="remove">
                                        <i class="fas fa-times"></i>
                                    </button>
                                </div>
                            </div>
                            <div class="card-body">
                                <div class="chartjs-size-monitor">
                                    <div class="chartjs-size-monitor-expand">
                                        <div class=""></div>
                                    </div>
                                    <div class="chartjs-size-monitor-shrink">
                                        <div class=""></div>
                                    </div>
                                </div>
                                <canvas id="pieChart"
                                        style="min-height: 250px; height: 250px; max-height: 250px; max-width: 100%; display: block;"
                                        width="439" height="250" class="chartjs-render-monitor"></canvas>
                            </div>
                            <!-- /.card-body -->
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-3">
                        <div class="card bg-gradient-success">
                            <div class="card-header border-0 ui-sortable-handle" style="cursor: move;">

                                <h3 class="card-title">
                                    <i class="far fa-calendar-alt"></i>
                                    Calendar
                                </h3>
                                <!-- tools card -->
                                <div class="card-tools">
                                    <!-- button with a dropdown -->
                                    <div class="btn-group">
                                        <button type="button" class="btn btn-success btn-sm dropdown-toggle"
                                                data-toggle="dropdown" data-offset="-52">
                                            <i class="fas fa-bars"></i>
                                        </button>
                                        <div class="dropdown-menu" role="menu">
                                            <a href="#" class="dropdown-item">Add new event</a>
                                            <a href="#" class="dropdown-item">Clear events</a>
                                            <div class="dropdown-divider"></div>
                                            <a href="#" class="dropdown-item">View calendar</a>
                                        </div>
                                    </div>
                                    <button type="button" class="btn btn-success btn-sm" data-card-widget="collapse">
                                        <i class="fas fa-minus"></i>
                                    </button>
                                    <button type="button" class="btn btn-success btn-sm" data-card-widget="remove">
                                        <i class="fas fa-times"></i>
                                    </button>
                                </div>
                                <!-- /. tools -->
                            </div>
                            <!-- /.card-header -->
                            <div class="card-body pt-0">
                                <!--The calendar -->
                                <div id="calendar" style="width: 100%">
                                    <div class="bootstrap-datetimepicker-widget usetwentyfour">
                                        <ul class="list-unstyled">
                                            <li class="show">
                                                <div class="datepicker">
                                                    <div class="datepicker-days" style="">
                                                        <table class="table table-sm">
                                                            <thead>
                                                            <tr>
                                                                <th class="prev" data-action="previous"><span
                                                                            class="fa fa-chevron-left"
                                                                            title="Previous Month"></span></th>
                                                                <th class="picker-switch" data-action="pickerSwitch"
                                                                    colspan="5" title="Select Month">April 2022
                                                                </th>
                                                                <th class="next" data-action="next"><span
                                                                            class="fa fa-chevron-right"
                                                                            title="Next Month"></span></th>
                                                            </tr>
                                                            <tr>
                                                                <th class="dow">Su</th>
                                                                <th class="dow">Mo</th>
                                                                <th class="dow">Tu</th>
                                                                <th class="dow">We</th>
                                                                <th class="dow">Th</th>
                                                                <th class="dow">Fr</th>
                                                                <th class="dow">Sa</th>
                                                            </tr>
                                                            </thead>
                                                            <tbody>
                                                            <tr>
                                                                <td data-action="selectDay" data-day="03/27/2022"
                                                                    class="day old weekend">27
                                                                </td>
                                                                <td data-action="selectDay" data-day="03/28/2022"
                                                                    class="day old">28
                                                                </td>
                                                                <td data-action="selectDay" data-day="03/29/2022"
                                                                    class="day old">29
                                                                </td>
                                                                <td data-action="selectDay" data-day="03/30/2022"
                                                                    class="day old">30
                                                                </td>
                                                                <td data-action="selectDay" data-day="03/31/2022"
                                                                    class="day old">31
                                                                </td>
                                                                <td data-action="selectDay" data-day="04/01/2022"
                                                                    class="day">1
                                                                </td>
                                                                <td data-action="selectDay" data-day="04/02/2022"
                                                                    class="day weekend">2
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td data-action="selectDay" data-day="04/03/2022"
                                                                    class="day weekend">3
                                                                </td>
                                                                <td data-action="selectDay" data-day="04/04/2022"
                                                                    class="day">4
                                                                </td>
                                                                <td data-action="selectDay" data-day="04/05/2022"
                                                                    class="day">5
                                                                </td>
                                                                <td data-action="selectDay" data-day="04/06/2022"
                                                                    class="day active today">6
                                                                </td>
                                                                <td data-action="selectDay" data-day="04/07/2022"
                                                                    class="day">7
                                                                </td>
                                                                <td data-action="selectDay" data-day="04/08/2022"
                                                                    class="day">8
                                                                </td>
                                                                <td data-action="selectDay" data-day="04/09/2022"
                                                                    class="day weekend">9
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td data-action="selectDay" data-day="04/10/2022"
                                                                    class="day weekend">10
                                                                </td>
                                                                <td data-action="selectDay" data-day="04/11/2022"
                                                                    class="day">11
                                                                </td>
                                                                <td data-action="selectDay" data-day="04/12/2022"
                                                                    class="day">12
                                                                </td>
                                                                <td data-action="selectDay" data-day="04/13/2022"
                                                                    class="day">13
                                                                </td>
                                                                <td data-action="selectDay" data-day="04/14/2022"
                                                                    class="day">14
                                                                </td>
                                                                <td data-action="selectDay" data-day="04/15/2022"
                                                                    class="day">15
                                                                </td>
                                                                <td data-action="selectDay" data-day="04/16/2022"
                                                                    class="day weekend">16
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td data-action="selectDay" data-day="04/17/2022"
                                                                    class="day weekend">17
                                                                </td>
                                                                <td data-action="selectDay" data-day="04/18/2022"
                                                                    class="day">18
                                                                </td>
                                                                <td data-action="selectDay" data-day="04/19/2022"
                                                                    class="day">19
                                                                </td>
                                                                <td data-action="selectDay" data-day="04/20/2022"
                                                                    class="day">20
                                                                </td>
                                                                <td data-action="selectDay" data-day="04/21/2022"
                                                                    class="day">21
                                                                </td>
                                                                <td data-action="selectDay" data-day="04/22/2022"
                                                                    class="day">22
                                                                </td>
                                                                <td data-action="selectDay" data-day="04/23/2022"
                                                                    class="day weekend">23
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td data-action="selectDay" data-day="04/24/2022"
                                                                    class="day weekend">24
                                                                </td>
                                                                <td data-action="selectDay" data-day="04/25/2022"
                                                                    class="day">25
                                                                </td>
                                                                <td data-action="selectDay" data-day="04/26/2022"
                                                                    class="day">26
                                                                </td>
                                                                <td data-action="selectDay" data-day="04/27/2022"
                                                                    class="day">27
                                                                </td>
                                                                <td data-action="selectDay" data-day="04/28/2022"
                                                                    class="day">28
                                                                </td>
                                                                <td data-action="selectDay" data-day="04/29/2022"
                                                                    class="day">29
                                                                </td>
                                                                <td data-action="selectDay" data-day="04/30/2022"
                                                                    class="day weekend">30
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td data-action="selectDay" data-day="05/01/2022"
                                                                    class="day new weekend">1
                                                                </td>
                                                                <td data-action="selectDay" data-day="05/02/2022"
                                                                    class="day new">2
                                                                </td>
                                                                <td data-action="selectDay" data-day="05/03/2022"
                                                                    class="day new">3
                                                                </td>
                                                                <td data-action="selectDay" data-day="05/04/2022"
                                                                    class="day new">4
                                                                </td>
                                                                <td data-action="selectDay" data-day="05/05/2022"
                                                                    class="day new">5
                                                                </td>
                                                                <td data-action="selectDay" data-day="05/06/2022"
                                                                    class="day new">6
                                                                </td>
                                                                <td data-action="selectDay" data-day="05/07/2022"
                                                                    class="day new weekend">7
                                                                </td>
                                                            </tr>
                                                            </tbody>
                                                        </table>
                                                    </div>
                                                    <div class="datepicker-months" style="display: none;">
                                                        <table class="table-condensed">
                                                            <thead>
                                                            <tr>
                                                                <th class="prev" data-action="previous"><span
                                                                            class="fa fa-chevron-left"
                                                                            title="Previous Year"></span></th>
                                                                <th class="picker-switch" data-action="pickerSwitch"
                                                                    colspan="5" title="Select Year">2022
                                                                </th>
                                                                <th class="next" data-action="next"><span
                                                                            class="fa fa-chevron-right"
                                                                            title="Next Year"></span></th>
                                                            </tr>
                                                            </thead>
                                                            <tbody>
                                                            <tr>
                                                                <td colspan="7"><span data-action="selectMonth"
                                                                                      class="month">Jan</span><span
                                                                            data-action="selectMonth"
                                                                            class="month">Feb</span><span
                                                                            data-action="selectMonth"
                                                                            class="month">Mar</span><span
                                                                            data-action="selectMonth"
                                                                            class="month active">Apr</span><span
                                                                            data-action="selectMonth"
                                                                            class="month">May</span><span
                                                                            data-action="selectMonth"
                                                                            class="month">Jun</span><span
                                                                            data-action="selectMonth"
                                                                            class="month">Jul</span><span
                                                                            data-action="selectMonth"
                                                                            class="month">Aug</span><span
                                                                            data-action="selectMonth"
                                                                            class="month">Sep</span><span
                                                                            data-action="selectMonth"
                                                                            class="month">Oct</span><span
                                                                            data-action="selectMonth"
                                                                            class="month">Nov</span><span
                                                                            data-action="selectMonth"
                                                                            class="month">Dec</span></td>
                                                            </tr>
                                                            </tbody>
                                                        </table>
                                                    </div>
                                                    <div class="datepicker-years" style="display: none;">
                                                        <table class="table-condensed">
                                                            <thead>
                                                            <tr>
                                                                <th class="prev" data-action="previous"><span
                                                                            class="fa fa-chevron-left"
                                                                            title="Previous Decade"></span></th>
                                                                <th class="picker-switch" data-action="pickerSwitch"
                                                                    colspan="5" title="Select Decade">2020-2029
                                                                </th>
                                                                <th class="next" data-action="next"><span
                                                                            class="fa fa-chevron-right"
                                                                            title="Next Decade"></span></th>
                                                            </tr>
                                                            </thead>
                                                            <tbody>
                                                            <tr>
                                                                <td colspan="7"><span data-action="selectYear"
                                                                                      class="year old">2019</span><span
                                                                            data-action="selectYear"
                                                                            class="year">2020</span><span
                                                                            data-action="selectYear"
                                                                            class="year">2021</span><span
                                                                            data-action="selectYear"
                                                                            class="year active">2022</span><span
                                                                            data-action="selectYear"
                                                                            class="year">2023</span><span
                                                                            data-action="selectYear"
                                                                            class="year">2024</span><span
                                                                            data-action="selectYear"
                                                                            class="year">2025</span><span
                                                                            data-action="selectYear"
                                                                            class="year">2026</span><span
                                                                            data-action="selectYear"
                                                                            class="year">2027</span><span
                                                                            data-action="selectYear"
                                                                            class="year">2028</span><span
                                                                            data-action="selectYear"
                                                                            class="year">2029</span><span
                                                                            data-action="selectYear" class="year old">2030</span>
                                                                </td>
                                                            </tr>
                                                            </tbody>
                                                        </table>
                                                    </div>
                                                    <div class="datepicker-decades" style="display: none;">
                                                        <table class="table-condensed">
                                                            <thead>
                                                            <tr>
                                                                <th class="prev" data-action="previous"><span
                                                                            class="fa fa-chevron-left"
                                                                            title="Previous Century"></span></th>
                                                                <th class="picker-switch" data-action="pickerSwitch"
                                                                    colspan="5">2000-2090
                                                                </th>
                                                                <th class="next" data-action="next"><span
                                                                            class="fa fa-chevron-right"
                                                                            title="Next Century"></span></th>
                                                            </tr>
                                                            </thead>
                                                            <tbody>
                                                            <tr>
                                                                <td colspan="7"><span data-action="selectDecade"
                                                                                      class="decade old"
                                                                                      data-selection="2006">1990</span><span
                                                                            data-action="selectDecade" class="decade"
                                                                            data-selection="2006">2000</span><span
                                                                            data-action="selectDecade" class="decade"
                                                                            data-selection="2016">2010</span><span
                                                                            data-action="selectDecade"
                                                                            class="decade active" data-selection="2026">2020</span><span
                                                                            data-action="selectDecade" class="decade"
                                                                            data-selection="2036">2030</span><span
                                                                            data-action="selectDecade" class="decade"
                                                                            data-selection="2046">2040</span><span
                                                                            data-action="selectDecade" class="decade"
                                                                            data-selection="2056">2050</span><span
                                                                            data-action="selectDecade" class="decade"
                                                                            data-selection="2066">2060</span><span
                                                                            data-action="selectDecade" class="decade"
                                                                            data-selection="2076">2070</span><span
                                                                            data-action="selectDecade" class="decade"
                                                                            data-selection="2086">2080</span><span
                                                                            data-action="selectDecade" class="decade"
                                                                            data-selection="2096">2090</span><span
                                                                            data-action="selectDecade"
                                                                            class="decade old" data-selection="2106">2100</span>
                                                                </td>
                                                            </tr>
                                                            </tbody>
                                                        </table>
                                                    </div>
                                                </div>
                                            </li>
                                            <li class="picker-switch accordion-toggle"></li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                            <!-- /.card-body -->
                        </div>
                    </div>
                    <div class="col-9">
                        <div class="card">
                            <div class="card-header ui-sortable-handle" style="cursor: move;">
                                <div class="row card-title col-12">
                                    <div class="col-4">
                                        <h3>
                                            <i class="ion ion-clipboard mr-1"></i>
                                            To Do List
                                        </h3>
                                    </div>
                                    <div class="col-6">
                                        <button type="button" class="btn btn-primary float-right"><i
                                                    class="fas fa-plus"></i>
                                            Add item
                                        </button>
                                    </div>
                                    <div class="col-2 card-tools">
                                        <ul class="pagination pagination-sm float-right">
                                            <li class="page-item"><a href="#" class="page-link">«</a></li>
                                            <li class="page-item"><a href="#" class="page-link">1</a></li>
                                            <li class="page-item"><a href="#" class="page-link">2</a></li>
                                            <li class="page-item"><a href="#" class="page-link">3</a></li>
                                            <li class="page-item"><a href="#" class="page-link">>></a></li>
                                        </ul>
                                    </div>
                                </div>

                            </div>
                            <!-- /.card-header -->
                            <div class="card-body" style="padding-bottom: 39px">
                                <ul class="todo-list ui-sortable" data-widget="todo-list">
                                    <li>
                                        <!-- drag handle -->
                                        <span class="handle ui-sortable-handle">
                      <i class="fas fa-ellipsis-v"></i>
                      <i class="fas fa-ellipsis-v"></i>
                    </span>
                                        <!-- checkbox -->
                                        <div class="icheck-primary d-inline ml-2">
                                            <input type="checkbox" value="" name="todo1" id="todoCheck1">
                                            <label for="todoCheck1"></label>
                                        </div>
                                        <!-- todo text -->
                                        <span class="text">Design a nice theme</span>
                                        <!-- Emphasis label -->
                                        <small class="badge badge-danger"><i class="far fa-clock"></i> 2 mins</small>
                                        <!-- General tools such as edit or delete-->
                                        <div class="tools">
                                            <i class="fas fa-edit"></i>
                                            <i class="fas fa-trash-o"></i>
                                        </div>
                                    </li>
                                    <li class="done">
                    <span class="handle ui-sortable-handle">
                      <i class="fas fa-ellipsis-v"></i>
                      <i class="fas fa-ellipsis-v"></i>
                    </span>
                                        <div class="icheck-primary d-inline ml-2">
                                            <input type="checkbox" value="" name="todo2" id="todoCheck2" checked="">
                                            <label for="todoCheck2"></label>
                                        </div>
                                        <span class="text">Make the theme responsive</span>
                                        <small class="badge badge-info"><i class="far fa-clock"></i> 4 hours</small>
                                        <div class="tools">
                                            <i class="fas fa-edit"></i>
                                            <i class="fas fa-trash-o"></i>
                                        </div>
                                    </li>
                                    <li>
                    <span class="handle ui-sortable-handle">
                      <i class="fas fa-ellipsis-v"></i>
                      <i class="fas fa-ellipsis-v"></i>
                    </span>
                                        <div class="icheck-primary d-inline ml-2">
                                            <input type="checkbox" value="" name="todo3" id="todoCheck3">
                                            <label for="todoCheck3"></label>
                                        </div>
                                        <span class="text">Let theme shine like a star</span>
                                        <small class="badge badge-warning"><i class="far fa-clock"></i> 1 day</small>
                                        <div class="tools">
                                            <i class="fas fa-edit"></i>
                                            <i class="fas fa-trash-o"></i>
                                        </div>
                                    </li>
                                    <li>
                    <span class="handle ui-sortable-handle">
                      <i class="fas fa-ellipsis-v"></i>
                      <i class="fas fa-ellipsis-v"></i>
                    </span>
                                        <div class="icheck-primary d-inline ml-2">
                                            <input type="checkbox" value="" name="todo4" id="todoCheck4">
                                            <label for="todoCheck4"></label>
                                        </div>
                                        <span class="text">Let theme shine like a star</span>
                                        <small class="badge badge-success"><i class="far fa-clock"></i> 3 days</small>
                                        <div class="tools">
                                            <i class="fas fa-edit"></i>
                                            <i class="fas fa-trash-o"></i>
                                        </div>
                                    </li>
                                    <li>
                    <span class="handle ui-sortable-handle">
                      <i class="fas fa-ellipsis-v"></i>
                      <i class="fas fa-ellipsis-v"></i>
                    </span>
                                        <div class="icheck-primary d-inline ml-2">
                                            <input type="checkbox" value="" name="todo5" id="todoCheck5">
                                            <label for="todoCheck5"></label>
                                        </div>
                                        <span class="text">Check your messages and notifications</span>
                                        <small class="badge badge-primary"><i class="far fa-clock"></i> 1 week</small>
                                        <div class="tools">
                                            <i class="fas fa-edit"></i>
                                            <i class="fas fa-trash-o"></i>
                                        </div>
                                    </li>
                                </ul>
                            </div>

                            <!-- /.card-body -->
                        </div>
                    </div>
                </div>
            </div><!-- /.container-fluid -->
        </section>
        <!-- /.content -->
    </div>
@endsection

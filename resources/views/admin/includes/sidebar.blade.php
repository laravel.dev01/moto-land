<aside class="main-sidebar sidebar-dark-primary elevation-4">
    <!-- Brand Logo -->
    <div class="brand-link">
        <i style='font-size:24px; padding: 0 6px 0 20px;' class='fas'>&#xf406;</i>
        <span class="brand-text font-weight-light">Admin</span>
        <a href="{{ route('logout') }}" style="padding-left: 10px"
           onclick="event.preventDefault();
           document.getElementById('logout-form').submit();">
            выйти
        </a>
        <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
            @csrf
        </form>
    </div>
    <!-- Sidebar -->
    <div class="sidebar pt-3">
        <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="true">
            <li class="nav-item">
{{--            <li class="nav-item menu-is-opening menu-open">--}}
                <a href="#" class="nav-link active">
                    <i class="nav-icon fa fa-list-ul"></i>
                    <p>
                        Структура
                        <i class="right fas fa-angle-left"></i>
                    </p>
                </a>
{{--                <ul class="nav nav-treeview" style="display: block;">--}}
                <ul class="nav nav-treeview" style="display: none;">
                    <li class="nav-item">
                        <a href="{{ route('admin.category.index') }}" class="nav-link">
                            <i class="nav-icon fa fa-indent"></i>
                            <p>Классы</p>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a href="{{ route('admin.models.index') }}" class="nav-link">
                            <i class="nav-icon fa fa-motorcycle"></i>
                            <p>Модели</p>
                        </a>
                    </li>
                </ul>
            </li>
            <li class="nav-item">
                <a href="{{ route('admin.stock.index') }}" class="nav-link">
                    <i class="nav-icon fas fa-warehouse"></i>
                    <p>
                        Склад
                    </p>
                </a>
            </li>
            <li class="nav-item">
                <a href="{{ route('admin.offer.index') }}" class="nav-link">
                    <i class="nav-icon fas fa-star"></i>
                    <p>
                        Спепредложения
                    </p>
                </a>
            </li>
        </ul>
    </div>
    <!-- /.sidebar -->
</aside>

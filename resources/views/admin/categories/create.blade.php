@extends('admin.layouts.main')
@section('title', $data['title'])
@section('content')
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <div class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <h1 class="m-0">{{$data['title']}}</h1>
                    </div><!-- /.col -->
                    <div class="col-sm-6">
                        <ol class="breadcrumb float-sm-right">
                            @foreach($data['breadcrumbs'] as $item)
                                @if(count($data['breadcrumbs'])>=2 && array_pop($data['breadcrumbs']))
                                    <li class="breadcrumb-item"><a href="{{$item['href']}}">{{$item['text']}}</a></li>
                                @else
                                    <li class="breadcrumb-item active">{{$item['text']}}</li>
                                @endif
                            @endforeach
                        </ol>
                    </div><!-- /.col -->
                </div><!-- /.row -->
            </div><!-- /.container-fluid -->
        </div>
        <!-- /.content-header -->

        <section class="content">
            <div class="container-fluid">
                <!-- /.row -->
{{--                <div class="row mb-3">--}}
{{--                    <div class="col-sm-4">--}}
{{--                        <form action="#"--}}
{{--                              method="POST"--}}
{{--                              enctype="multipart/form-data">--}}
{{--                            @csrf--}}
{{--                            <div class="input-group">--}}
{{--                                <div class="custom-file">--}}
{{--                                    <input type="file"--}}
{{--                                           name="import_xls"--}}
{{--                                           class="custom-file-input"--}}
{{--                                           id="importTth">--}}
{{--                                    <label class="custom-file-label"--}}
{{--                                           for="importTth">ЗАГРУЗИТЬ ФАЙЛ XLS</label>--}}
{{--                                </div>--}}
{{--                                <div class="input-group-append">--}}
{{--                                    <button type="submit"--}}
{{--                                            class="btn btn-primary">--}}
{{--                                        Загрузить--}}
{{--                                    </button>--}}
{{--                                </div>--}}
{{--                            </div>--}}
{{--                        </form>--}}
{{--                    </div>--}}
{{--                    <div class="col">--}}
{{--                        <a class="btn btn-primary" href="#">--}}
{{--                            ВЫГРУЗИТЬФАЙЛ XLS--}}
{{--                        </a>--}}
{{--                    </div>--}}
{{--                </div>--}}
                <category-item-component :path="'{{route('admin.category.index')}}'"></category-item-component>
            </div><!-- /.container-fluid -->
        </section>
        <!-- /.content -->
    </div>
@endsection
@extends('admin.layouts.main')
@section('title', $data['title'])
@section('content')
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <div class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <h1 class="m-0">{{$data['title']}}</h1>
                    </div><!-- /.col -->
                    <div class="col-sm-6">
                        <ol class="breadcrumb float-sm-right">
                            @foreach($data['breadcrumbs'] as $item)
                                @if(count($data['breadcrumbs'])>=2 && array_pop($data['breadcrumbs']))
                                    <li class="breadcrumb-item"><a href="{{$item['href']}}">{{$item['text']}}</a></li>
                                @else
                                    <li class="breadcrumb-item active">{{$item['text']}}</li>
                                @endif
                            @endforeach
                        </ol>
                    </div><!-- /.col -->
                </div><!-- /.row -->
            </div><!-- /.container-fluid -->
        </div>
        <!-- /.content-header -->

        <!-- Main content -->
        <section class="content">
            <div class="container-fluid">
                <!-- Small boxes (Stat box) -->
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-header row">
                                <div class="col-2"><h3 class="card-title">Класс мотоцикла</h3></div>
                                <div class="col-4"><h3 class="card-title">Доступные модели</h3></div>
                            </div>
                            <!-- ./card-header -->
                            <div class="card-body p-0">
                                <table class="table table-hover">
                                    <tbody>
                                    @foreach($categories as $category)
                                        <tr data-widget="expandable-table" aria-expanded="false">
                                            <td>
                                                <div class="row">
                                                    <div class="col-2">
                                                        <i class="expandable-table-caret fas fa-caret-right fa-fw"></i>
                                                        {{$category->category_name}}
                                                    </div>
                                                    <div class="col-4">
                                                        {{count($category->models)}}
                                                    </div>
                                                </div>
                                            </td>
                                        </tr>
                                        <tr class="expandable-body">
                                            <td>
                                                <div class="p-0" style="">
                                                    <table class="table table-hover">
                                                        <tbody>
                                                        @foreach($category->models as $model)
                                                            <tr data-widget="expandable-table" aria-expanded="false">
                                                                <td class="row">
                                                                    <div class="col-2"><i
                                                                                class="expandable-table-caret fas fa-caret-right fa-fw"></i>
                                                                        {{$model['name']}}
                                                                    </div>
                                                                </td>
                                                            </tr>
                                                            <tr class="expandable-body d-none">
                                                                <td>
                                                                    <div class="p-0" style="display: none;">
                                                                        <table class="table table-hover">
                                                                            <tbody>
                                                                            <tr class="row">
                                                                                <td class="col-2">
                                                                                    <img
                                                                                            src="{{$model['image']}}"
                                                                                            alt="" width="170px">
                                                                                </td>
                                                                                <td class="col-4 row">
                                                                                    <div class="col-12">
                                                                                        <form action="{{ route('admin.tth.import', $model->id) }}"
                                                                                              method="POST"
                                                                                              enctype="multipart/form-data">
                                                                                            @csrf
                                                                                            <div class="input-group">
                                                                                                <div class="custom-file">
                                                                                                    <input type="file"
                                                                                                           name="import_xls"
                                                                                                           class="custom-file-input"
                                                                                                           id="importTth">
                                                                                                    <label class="custom-file-label"
                                                                                                           for="importTth">ЗАГРУЗИТЬ
                                                                                                        ФАЙЛ XLS C
                                                                                                        ТТХ</label>
                                                                                                </div>
                                                                                                <div class="input-group-append">
                                                                                                    <button type="submit"
                                                                                                            class="btn btn-primary">
                                                                                                        Загрузить
                                                                                                    </button>
                                                                                                </div>
                                                                                            </div>
                                                                                        </form>
                                                                                    </div>
                                                                                    <div class="col-12"><a
                                                                                                href="{{ route('admin.tth.export', $model->id) }}">ВЫГРУЗИТЬ
                                                                                            ФАЙЛ XLS C ТТХ</a></div>
                                                                                </td>
                                                                            </tr>
                                                                            {{--                                                                    <tr>--}}
                                                                            {{--                                                                        <td>219-1-2</td>--}}
                                                                            {{--                                                                    </tr>--}}
                                                                            {{--                                                                    <tr>--}}
                                                                            {{--                                                                        <td>219-1-3</td>--}}
                                                                            {{--                                                                    </tr>--}}
                                                                            </tbody>
                                                                        </table>
                                                                    </div>
                                                                </td>
                                                            </tr>
                                                        @endforeach
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>
                            <!-- /.card-body -->
                        </div>
                        <!-- /.card -->
                    </div>
                </div>
                <!-- /.row -->

            </div><!-- /.container-fluid -->
        </section>
        <!-- /.content -->
    </div>
    <style>
        .custom-file-input:lang(en) ~ .custom-file-label::after {
            content: "Выбрать";
        }
    </style>
@endsection

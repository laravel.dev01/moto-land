<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>MOTO-LAND.RU</title>

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}" defer></script>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
</head>
<body>
<video autoplay loop muted class="bg" id="bgvideo">
{{--    <source src="https://videos.ctfassets.net/x7j9qwvpvr5s/27ildjCpYX7l96PxGtd9Ik/c63ec50870d1766a2e26ece2f74874eb/Loop10secs_website_v1.mp4" type="video/mp4"></source>--}}
    <source src="{{ asset('assets/video/video.mp4') }}" type="video/mp4"></source>
</video>
<div class="info">
    Сайт находится в стадии разработки!
</div>
</body>
</html>

<style>
    .bg {
        position: fixed;
        right: 0;
        bottom: 0;
        min-width: 100%;
        min-height: 100%;
        width: auto;
        height: auto;
        z-index: -9999;
        filter: blur(5px);
    }
    .info{
        position: absolute;
        top: 50%;
        left: 50%;
        transform: translate(-50% ,-50%);
        color: #fff;
        font-size: 38px;
        text-align: center;
        font-family: unset;
    }
</style>

import Vue from 'vue';
import CreateComponent from "./components/Offer/CreateComponent";
import EditComponent from "./components/Offer/EditComponent";
import StockComponent from "./components/Stock/StockComponent";
import VisitComponent from "./components/manager/VisitComponent";
import StockItemComponent from "./components/Stock/StockItemComponent";
import StockSearchComponent from "./components/Stock/StockSearchComponent";
import CategoryComponent from "./components/Category/CategoryComponent";
import CategoryItemComponent from "./components/Category/CategoryItemComponent";
import CategoryItemEditComponent from "./components/Category/CategoryItemEditComponent";
require('./bootstrap');



const app = new Vue({
    el:'#app',
    components:{
        CreateComponent,
        EditComponent,
        StockComponent,
        VisitComponent,
        StockItemComponent,
        StockSearchComponent,
        CategoryComponent,
        CategoryItemComponent,
        CategoryItemEditComponent
    }
})


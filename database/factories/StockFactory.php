<?php

namespace Database\Factories;

use App\Models\ModelMoto;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Facades\DB;

class StockFactory extends Factory
{


    protected $table = 'stocks';
    protected $model = \App\Models\Stock::class;


     public function definition()
    {

        return [
            'vin'=>$this->faker->regexify('[A-Z0-9]{17}'),
            'engine'=>$this->faker->regexify('[A-Z0-9]{6}'),
            'name'=> ' ',
            'color_id'=>$this->faker->numberBetween(51,300),
            'options'=>$this->faker->regexify('[A-Z0-9]{3}'),
            'model_moto_id'=>$this->faker->numberBetween(1,6),
            'price'=>$this->faker->numberBetween(500000,1200000),
            'special_price'=>$this->faker->numberBetween(500000,1200000),
            'trade_in'=>$this->faker->numberBetween(5000,10000),
            'kasko'=>$this->faker->numberBetween(5000,10000),
            'credit'=>$this->faker->numberBetween(5000,10000),
            'leasing'=>$this->faker->numberBetween(5000,10000),
            'offer'=>$this->faker->text(),
            'status'=>$this->faker->numberBetween(0,1),
        ];
    }
}

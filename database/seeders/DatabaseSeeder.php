<?php

namespace Database\Seeders;

use App\Models\Color;
use App\Models\Stock;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
//        Color::factory()->count(50)->create();
//        Stock::factory()->count(100)->create();
    }

//    public function run()
//    {
//
//        Stock::factory()->count(21)->create();
////        DB::table('tths')->insert(
////
////            [
////                'id'                 => 13,
////                'model_moto_id'      => 1,
////                'engine_capacity'    => '1200',
////                'engine_power'       => '116',
////                'torque'             => '123',
////                'cylinders_valves'   => '2/4',
////                'maximum_speed'      => 190,
////                'fuel_consumption'   => 100,
////                'its_mass'           => 200,
////                'permissible_weight' => 250,
////            ]);
////        DB::table('model_motos')->insert(
////            [
////                [
////                    'id'          => 1,
////                    'name'        => 'R 1250 GS',
////                    'category_id' => 5,
////                    'image'       => 'https://cdn.kodixauto.ru/media/image/6124fbd5e87c6d58b084cdd3',
////                ],
////                [
////                    'id'          => 2,
////                    'name'        => 'R 1250 GS Adv',
////                    'category_id' => 5,
////                    'image'       => 'https://cdn.kodixauto.ru/media/image/6124fc09e87c6d58b084cde8',
////                ],
////                [
////                    'id'          => 3,
////                    'name'        => 'S 1000 XR',
////                    'category_id' => 5,
////                    'image'       => 'https://cdn.kodixauto.ru/media/image/6124fbf1a5e47913821121d5',
////                ],
////                [
////                    'id'          => 4,
////                    'name'        => 'F 750 GS',
////                    'category_id' => 5,
////                    'image'       => 'https://cdn.kodixauto.ru/media/image/6124fc15e87c6d58b084cdef',
////                ],
////                [
////                    'id'          => 5,
////                    'name'        => 'F 850 GS',
////                    'category_id' => 5,
////                    'image'       => 'https://cdn.kodixauto.ru/media/image/6124fc1ee87c6d58b084cdf1',
////                ],
////                [
////                    'id'          => 6,
////                    'name'        => 'R nineT Pure',
////                    'category_id' => 4,
////                    'image'       => 'https://cdn.kodixauto.ru/media/image/61783cea20c4dccb5bfda396',
////                ],
////                [
////                    'id'          => 7,
////                    'name'        => 'R nineT Scrambler',
////                    'category_id' => 4,
////                    'image'       => 'https://cdn.kodixauto.ru/media/image/61783d02cd5c20b5777c7800',
////                ],
////                [
////                    'id'          => 8,
////                    'name'        => 'R 18 Transcontinental',
////                    'category_id' => 4,
////                    'image'       => 'https://cdn.kodixauto.ru/media/image/62177991cc435044e7a1b5c2',
////                ],
////                [
////                    'id'          => 9,
////                    'name'        => 'G 310 R',
////                    'category_id' => 3,
////                    'image'       => 'https://cdn.kodixauto.ru/media/image/6124fc9be87c6d58b084ce0c',
////                ],
////                [
////                    'id'          => 10,
////                    'name'        => 'F 900 R',
////                    'category_id' => 3,
////                    'image'       => 'https://cdn.kodixauto.ru/media/image/6124fdb5e87c6d58b084cf1a',
////                ],
////                [
////                    'id'          => 11,
////                    'name'        => 'S 1000 R',
////                    'category_id' => 3,
////                    'image'       => 'https://cdn.kodixauto.ru/media/image/6124fde8e87c6d58b084cf2c',
////                ],
////                [
////                    'id'          => 12,
////                    'name'        => 'R 1250 R',
////                    'category_id' => 3,
////                    'image'       => 'https://cdn.kodixauto.ru/media/image/6124fe03e87c6d58b084cf2d',
////                ],
////                [
////                    'id'          => 13,
////                    'name'        => 'R 1250 RS',
////                    'category_id' => 1,
////                    'image'       => 'https://cdn.kodixauto.ru/media/image/6124ff84a5e4791382112214',
////                ],
////                [
////                    'id'          => 14,
////                    'name'        => 'M 1000 RR',
////                    'category_id' => 1,
////                    'image'       => 'https://cdn.kodixauto.ru/media/image/6124ff94a5e479138211221d',
////                ],
////                [
////                    'id'          => 15,
////                    'name'        => 'S 1000 RR',
////                    'category_id' => 1,
////                    'image'       => 'https://cdn.kodixauto.ru/media/image/6124ffa7a5e479138211221e',
////                ],
////                [
////                    'id'          => 16,
////                    'name'        => 'K 1600 GT',
////                    'category_id' => 2,
////                    'image'       => 'https://cdn.kodixauto.ru/media/image/6124fedde87c6d58b084cf4d',
////                ],
////                [
////                    'id'          => 17,
////                    'name'        => 'K 1600 GTL',
////                    'category_id' => 2,
////                    'image'       => 'https://cdn.kodixauto.ru/media/image/6124fee9e87c6d58b084cf4e',
////                ],
////                [
////                    'id'          => 18,
////                    'name'        => 'R 1250 RT',
////                    'category_id' => 2,
////                    'image'       => 'https://cdn.kodixauto.ru/media/image/6124ff48a5e4791382112210',
////                ],
////                [
////                    'id'          => 19,
////                    'name'        => 'K 1600 Bagger',
////                    'category_id' => 2,
////                    'image'       => 'https://cdn.kodixauto.ru/media/image/6124ff74a5e4791382112211',
////                ],
////                [
////                    'id'          => 20,
////                    'name'        => 'G 400 GT',
////                    'category_id' => 6,
////                    'image'       => 'https://cdn.kodixauto.ru/media/image/61783a75cd5c20b5777c777d',
////                ],
////                [
////                    'id'          => 201,
////                    'name'        => 'G 400 X',
////                    'category_id' => 6,
////                    'image'       => 'https://cdn.kodixauto.ru/media/image/61783aadcd5c20b5777c777f',
////                ],
////            ]
////        );
//    }
}

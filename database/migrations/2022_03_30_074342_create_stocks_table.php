<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateStocksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('stocks', function (Blueprint $table) {
            $table->id();
            $table->string('vin');
            $table->string('engine');
            $table->string('name');
            $table->foreignId('color_id')->constrained();
            $table->string('options')->nullable()->default(null);
            $table->foreignId('model_moto_id')->constrained();
            $table->integer('price')->nullable()->default(0);
            $table->integer('special_price')->nullable()->default(0);
            $table->integer('trade_in')->nullable()->default(0);
            $table->integer('kasko')->nullable()->default(0);
            $table->integer('credit')->nullable()->default(0);
            $table->integer('leasing')->nullable()->default(0);
            $table->text('offer')->nullable()->default(null);
            $table->integer('status')->nullable()->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('stocks');
    }
}

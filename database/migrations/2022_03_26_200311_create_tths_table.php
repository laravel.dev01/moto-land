<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTthsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tths', function (Blueprint $table) {
            $table->id();
            $table->foreignId('model_moto_id')->constrained();
            $table->string('engine_capacity')->nullable();
            $table->string('engine_power')->nullable();
            $table->string('torque')->nullable();
            $table->string('cylinders_valves')->nullable();
            $table->integer('maximum_speed')->nullable();
            $table->integer('fuel_consumption')->nullable();
            $table->integer('its_mass')->nullable();
            $table->integer('permissible_weight')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tths');
    }
}
